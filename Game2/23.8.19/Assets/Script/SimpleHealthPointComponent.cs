﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class  SimpleHealthPointComponent : MonoBehaviour
{
    [SerializeField]
    public const float MAX_HP = 100;
    
    [SerializeField]
    private float _healthPoint;

    public float HealthPoint
    {
         get
        { 
            return _healthPoint;
        } 
        set
        {
            if(value > 0) 
                { if(value <= MAX_HP)
                    { 
                        _healthPoint = value; 
                    }
                    else
                    { 
                        _healthPoint = MAX_HP; 
                    }
                }
        }
             
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
