﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

public class PlayerCollisionCheck : MonoBehaviour
{
     TextMeshPro _textMeshPro = null;

    // Start is called before the first frame update
    void Start()
    {
        _textMeshPro = this.GetComponentInChildren<TextMeshPro>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Item")
        { 
          Destroy(collision.gameObject);
        }
        else if (collision.gameObject.tag == "Obstacle") 
        {
          Destroy(collision.gameObject, 2f);
        } 

        ItemTypeComponent itc = collision.gameObject.GetComponent<ItemTypeComponent>();
        if(itc != null)
          {
             switch(itc.Type)
               {
                    case ItemType.SPHERE_ITEM:
                         _textMeshPro.text = "Sphere Item";
                    break;
                    case ItemType.CYLINDER_OBSTACLE:
                         _textMeshPro.text = "Cylinder Obstacle";
                    break;
                    case ItemType.CUBE_OBSTACLE:
                         _textMeshPro.text = "Cube Obstacle";
                    break;
                    case ItemType. CAPSULE_OBSTACLE:
                         _textMeshPro.text = "Capsule Obstacle";
                    break;
               }

          }
          
     }
}
